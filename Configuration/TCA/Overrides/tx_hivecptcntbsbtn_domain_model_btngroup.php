<?php

defined('TYPO3_MODE') or die();

$sModel = 'tx_hivecptcntbsbtn_domain_model_btngroup';

$GLOBALS['TCA'][$sModel]['columns']['btn_group']['config']['foreign_table_where'] = 'AND tx_hivecptcntbsbtn_domain_model_btngroup.uid <> ###THIS_UID###';
$GLOBALS['TCA'][$sModel]['columns']['btn_group']['config']['type'] = 'select';
$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:hive_cpt_cnt_bs_btn/Resources/Public/Icons/bootstrap_16x16.gif';