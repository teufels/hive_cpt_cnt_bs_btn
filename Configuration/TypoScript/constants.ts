
plugin.tx_hivecptcntbsbtn_hivecptcntbsbtnrender {
    view {
        # cat=plugin.tx_hivecptcntbsbtn_hivecptcntbsbtnrender/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_cnt_bs_btn/Resources/Private/Templates/
        # cat=plugin.tx_hivecptcntbsbtn_hivecptcntbsbtnrender/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_cnt_bs_btn/Resources/Private/Partials/
        # cat=plugin.tx_hivecptcntbsbtn_hivecptcntbsbtnrender/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_cnt_bs_btn/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptcntbsbtn_hivecptcntbsbtnrender//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_hivecptcntbsbtn_hivecptcntbsbtndocumentation {
    view {
        # cat=plugin.tx_hivecptcntbsbtn_hivecptcntbsbtndocumentation/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_cnt_bs_btn/Resources/Private/Templates/
        # cat=plugin.tx_hivecptcntbsbtn_hivecptcntbsbtndocumentation/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_cnt_bs_btn/Resources/Private/Partials/
        # cat=plugin.tx_hivecptcntbsbtn_hivecptcntbsbtndocumentation/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_cnt_bs_btn/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptcntbsbtn_hivecptcntbsbtndocumentation//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin.tx_hivecptcntbsbtn_hivecptcntbsbtnrender {
    settings {
        bDebug = 0
        sDebugIp = 192.168.99.1,80.152.201.114
    }
}

plugin {
    tx_hivecptcntbsbtn {
        model {
            HIVE\HiveCptCntBsBtn\Domain\Model\Btn {
                persistence {
                    storagePid =
                }
            }
        }
    }
}