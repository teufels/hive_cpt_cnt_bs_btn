<?php
namespace HIVE\HiveCptCntBsBtn\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class BtnGroupTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveCptCntBsBtn\Domain\Model\BtnGroup
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveCptCntBsBtn\Domain\Model\BtnGroup();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getBackendTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getBackendTitle()
        );

    }

    /**
     * @test
     */
    public function setBackendTitleForStringSetsBackendTitle()
    {
        $this->subject->setBackendTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'backendTitle',
            $this->subject
        );

    }

    /**
     * @test
     */
    public function getDropdownTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDropdownTitle()
        );

    }

    /**
     * @test
     */
    public function setDropdownTitleForStringSetsDropdownTitle()
    {
        $this->subject->setDropdownTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'dropdownTitle',
            $this->subject
        );

    }

    /**
     * @test
     */
    public function getDropdownHeaderReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDropdownHeader()
        );

    }

    /**
     * @test
     */
    public function setDropdownHeaderForStringSetsDropdownHeader()
    {
        $this->subject->setDropdownHeader('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'dropdownHeader',
            $this->subject
        );

    }

    /**
     * @test
     */
    public function getDropdownReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getDropdown()
        );

    }

    /**
     * @test
     */
    public function setDropdownForBoolSetsDropdown()
    {
        $this->subject->setDropdown(true);

        self::assertAttributeEquals(
            true,
            'dropdown',
            $this->subject
        );

    }

    /**
     * @test
     */
    public function getClassSizeReturnsInitialValueForInt()
    {
    }

    /**
     * @test
     */
    public function setClassSizeForIntSetsClassSize()
    {
    }

    /**
     * @test
     */
    public function getVerticalReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getVertical()
        );

    }

    /**
     * @test
     */
    public function setVerticalForBoolSetsVertical()
    {
        $this->subject->setVertical(true);

        self::assertAttributeEquals(
            true,
            'vertical',
            $this->subject
        );

    }

    /**
     * @test
     */
    public function getJustifyReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getJustify()
        );

    }

    /**
     * @test
     */
    public function setJustifyForBoolSetsJustify()
    {
        $this->subject->setJustify(true);

        self::assertAttributeEquals(
            true,
            'justify',
            $this->subject
        );

    }

    /**
     * @test
     */
    public function getBtnGroupReturnsInitialValueForBtnGroup()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getBtnGroup()
        );

    }

    /**
     * @test
     */
    public function setBtnGroupForObjectStorageContainingBtnGroupSetsBtnGroup()
    {
        $btnGroup = new \HIVE\HiveCptCntBsBtn\Domain\Model\BtnGroup();
        $objectStorageHoldingExactlyOneBtnGroup = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneBtnGroup->attach($btnGroup);
        $this->subject->setBtnGroup($objectStorageHoldingExactlyOneBtnGroup);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneBtnGroup,
            'btnGroup',
            $this->subject
        );

    }

    /**
     * @test
     */
    public function addBtnGroupToObjectStorageHoldingBtnGroup()
    {
        $btnGroup = new \HIVE\HiveCptCntBsBtn\Domain\Model\BtnGroup();
        $btnGroupObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $btnGroupObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($btnGroup));
        $this->inject($this->subject, 'btnGroup', $btnGroupObjectStorageMock);

        $this->subject->addBtnGroup($btnGroup);
    }

    /**
     * @test
     */
    public function removeBtnGroupFromObjectStorageHoldingBtnGroup()
    {
        $btnGroup = new \HIVE\HiveCptCntBsBtn\Domain\Model\BtnGroup();
        $btnGroupObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $btnGroupObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($btnGroup));
        $this->inject($this->subject, 'btnGroup', $btnGroupObjectStorageMock);

        $this->subject->removeBtnGroup($btnGroup);

    }

    /**
     * @test
     */
    public function getBtnReturnsInitialValueForBtn()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getBtn()
        );

    }

    /**
     * @test
     */
    public function setBtnForObjectStorageContainingBtnSetsBtn()
    {
        $btn = new \HIVE\HiveCptCntBsBtn\Domain\Model\Btn();
        $objectStorageHoldingExactlyOneBtn = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneBtn->attach($btn);
        $this->subject->setBtn($objectStorageHoldingExactlyOneBtn);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneBtn,
            'btn',
            $this->subject
        );

    }

    /**
     * @test
     */
    public function addBtnToObjectStorageHoldingBtn()
    {
        $btn = new \HIVE\HiveCptCntBsBtn\Domain\Model\Btn();
        $btnObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $btnObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($btn));
        $this->inject($this->subject, 'btn', $btnObjectStorageMock);

        $this->subject->addBtn($btn);
    }

    /**
     * @test
     */
    public function removeBtnFromObjectStorageHoldingBtn()
    {
        $btn = new \HIVE\HiveCptCntBsBtn\Domain\Model\Btn();
        $btnObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $btnObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($btn));
        $this->inject($this->subject, 'btn', $btnObjectStorageMock);

        $this->subject->removeBtn($btn);

    }
}
